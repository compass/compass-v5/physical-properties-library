from math import log, exp
from physics.properties import BothProperties

# diphasic : valid between -50C and 200C
def diphasic_psat_with_derivative(X, dfdX):
    Psat = 100.0 * exp(46.784 - 6435.0 / X.temperature - 3.868 * log(X.temperature))
    dfdX.temperature = (6435.0 / X.temperature**2.0 - 3.868 / X.temperature) * Psat
    return Psat


# diphasic : valid between -50C and 200C
def diphasic_psat_without_derivative(X):
    return 100.0 * exp(46.784 - 6435.0 / X.temperature - 3.868 * log(X.temperature))


diphasic_psat = BothProperties(
    with_derivatives=diphasic_psat_with_derivative,
    without_derivatives=diphasic_psat_without_derivative,
)


def psat_error_with_derivative(X, dfdX):
    return 1 / 0  # can be compiled ???


def psat_error_without_derivative(X):
    return 1 / 0  # can be compiled ???


error_psat = BothProperties(
    with_derivatives=psat_error_with_derivative,
    without_derivatives=psat_error_without_derivative,
)
