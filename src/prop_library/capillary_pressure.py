import numpy as np
from physics.properties import BothProperties


# Xsat structure
def no_pc_without_derivative(X):
    return 0.0


def no_pc_with_derivative(X, ddfX):  # Xsat struct
    ddfX.saturation.fill(0)
    return 0.0


no_capillary_pressure = BothProperties(
    with_derivatives=no_pc_with_derivative,
    without_derivatives=no_pc_without_derivative,
)

# hypothesis used in van Genuchten definition
def assert_diphasic_phases_indexes(physics):
    # assumed in phase pressure definition
    assert physics.Phase.gas == 0
    assert physics.Phase.liquid == 1


def vanGenuchten_pc(Pr, Slr, Sgr, n, Slb_reg=0.99):
    minus_one_over_m = n / (1.0 - n)  # usualy we define m = 1 - 1/n and slb ** (-1/m)
    # the law is regularized to allow for Sg = Sgr
    Pc1 = Pr * (Slb_reg**minus_one_over_m - 1.0) ** (1.0 / n)
    alpha = Pc1 / (Slb_reg - 1.0)
    eps = 1.0e-7  # eps is not so small because the Pc is high

    def Pc_without_derivatives(X):
        Slb = (1.0 - X.saturation[0] - Slr) / (1.0 - Sgr - Slr)

        assert Slb > eps  # stay at a certain distance of the asymptote

        if Slb < Slb_reg:  # Sg far enough from Sgr
            return Pr * (Slb**minus_one_over_m - 1.0) ** (1.0 / n)
        else:  # Sg close or equal to Sgr
            # the law is regularized to allow for Sg = Sgr
            return alpha * (Slb - Slb_reg) + Pc1
        # if Slb > 1.0 :  # maybe useful ?
        #     return 1.e15

    def Pc_with_derivatives(X, ddfX):  # Xsat struct
        ddfX.saturation[1] = 0  # derivative wrt Sl
        Slb = (1.0 - X.saturation[0] - Slr) / (1.0 - Sgr - Slr)
        dSlbdS = -1.0 / (1.0 - Sgr - Slr)

        assert Slb > eps  # stay at a certain distance of the asymptote

        if Slb < Slb_reg:  # Sg far enough from Sgr
            # derivative wrt Sg
            ddfX.saturation[0] = (
                Pr
                * minus_one_over_m
                / n
                * dSlbdS
                * Slb ** (minus_one_over_m - 1.0)
                * (Slb**minus_one_over_m - 1.0) ** (1.0 / n - 1.0)
            )
            return Pr * (Slb**minus_one_over_m - 1.0) ** (1.0 / n)
        else:  # Sg close or equal to Sgr
            # the law is regularized to allow for Sg = Sgr
            ddfX.saturation[0] = alpha * dSlbdS
            return alpha * (Slb - Slb_reg) + Pc1
        # if Slb > 1.0 :  # maybe useful ?
        #     ddfX.saturation[0] = 0
        #     return 1.e15

    return BothProperties(
        with_derivatives=Pc_with_derivatives,
        without_derivatives=Pc_without_derivatives,
    )
