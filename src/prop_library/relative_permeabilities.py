import numpy as np
from physics.properties import BothProperties


def gas_diphasic_rel_perm_with_derivatives(X, dfdX):
    dfdX.saturation[1] = 0
    dfdX.saturation[0] = 2 * X.saturation[0]
    return X.saturation[0] ** 2


def gas_diphasic_rel_perm_without_derivatives(X):
    return X.saturation[0] ** 2


gas_diphasic_rel_perm = BothProperties(
    with_derivatives=gas_diphasic_rel_perm_with_derivatives,
    without_derivatives=gas_diphasic_rel_perm_without_derivatives,
)


def liquid_diphasic_rel_perm_with_derivatives(X, dfdX):
    dfdX.saturation[0] = 0
    dfdX.saturation[1] = 2 * X.saturation[1]
    return X.saturation[1] ** 2


def liquid_diphasic_rel_perm_without_derivatives(X):
    return X.saturation[1] ** 2


liquid_diphasic_rel_perm = BothProperties(
    with_derivatives=liquid_diphasic_rel_perm_with_derivatives,
    without_derivatives=liquid_diphasic_rel_perm_without_derivatives,
)


# van Genuchten relative permeabilities
# The relative permeability of the liquid phase is expressed by integrating
# the Mualem prediction model in the van Genuchten capillarity model
def vanGenuchten_kr(Pr, Slr, Sgr, n, Sl_reg=0.99):
    m = 1.0 - 1.0 / n
    scale = 1.0 / (1.0 - Slr - Sgr)
    Slb_reg = (Sl_reg - Slr) * scale
    krl_reg = (1.0 - (1.0 - Slb_reg ** (1.0 / m)) ** m) ** 2 * np.sqrt(Slb_reg)
    reg_slope = (1.0 - krl_reg) / (1.0 - Sl_reg)
    reg_origin = (krl_reg - Sl_reg) / (1.0 - Sl_reg)

    def gas_rel_perm_without_derivatives(X):
        Sg = X.saturation[0]
        Slb = (1.0 - Sg - Slr) * scale
        if Slb <= 0.0:  # Sg >= 1-Slr
            return 1.0
        elif Slb < 1.0:
            return (1.0 - Slb ** (1.0 / m)) ** (2.0 * m) * np.sqrt(1.0 - Slb)
        else:
            return 0.0  # Sg <= to Sgr

    def gas_rel_perm_with_derivatives(X, dfdX):
        Sg = X.saturation[0]
        dfdX.saturation[1] = 0  # derivatives wrt Sl = 0
        Slb = (1.0 - Sg - Slr) * scale
        if Slb <= 0.0:  # Sg >= 1-Slr
            dfdX.saturation[0] = 0.0
            return 1.0
        elif Slb < 1.0:
            ss = 1.0 - Slb ** (1.0 / m)
            dfdX.saturation[0] = scale * (
                2 * Slb ** (1.0 / m - 1.0) * ss ** (2.0 * m - 1.0) * np.sqrt(1.0 - Slb)
                + ss ** (2.0 * m) / 2.0 / np.sqrt(1.0 - Slb)
            )
            return (1.0 - Slb ** (1.0 / m)) ** (2.0 * m) * np.sqrt(1.0 - Slb)
        else:
            dfdX.saturation[0] = 0.0
            return 0.0  # Sg <= to Sgr

    gas_rel_perm = BothProperties(
        with_derivatives=gas_rel_perm_with_derivatives,
        without_derivatives=gas_rel_perm_without_derivatives,
    )

    def liquid_rel_perm_without_derivatives(X):
        Sl = X.saturation[1]
        Slb = (Sl - Slr) * scale
        if Slb <= 0.0:  # Sl <= Slr
            return 0.0
        elif Slb < Slb_reg:  # Sl far enough from regularization
            return (1.0 - (1.0 - Slb ** (1.0 / m)) ** m) ** 2 * np.sqrt(Slb)
        else:  # Sl < 1 - Sgr
            # regularization : line such that f(Sl_reg) = krl_reg
            return reg_slope * Sl + reg_origin

    def liquid_rel_perm_with_derivatives(X, dfdX):
        Sl = X.saturation[1]
        dfdX.saturation[0] = 0  # derivatives wrt Sg = 0
        Slb = (Sl - Slr) * scale
        if Slb <= 0.0:  # Sl <= Slr
            dfdX.saturation[1] = 0.0
            return 0.0
        elif Slb < Slb_reg:  # Sl far enough from regularization
            ss = 1.0 - (1.0 - Slb ** (1.0 / m)) ** m
            ds = Slb ** (1.0 / m - 1.0) * (1.0 - Slb ** (1.0 / m)) ** (m - 1.0)
            dfdX.saturation[1] = scale * (
                2.0 * ds * ss * np.sqrt(Slb) + ss**2 / 2.0 / np.sqrt(Slb)
            )
            return (1.0 - (1.0 - Slb ** (1.0 / m)) ** m) ** 2 * np.sqrt(Slb)
        else:  # Sl < 1 - Sgr
            # regularization : line such that f(Sl_reg) = krl_reg
            dfdX.saturation[1] = reg_slope
            return reg_slope * Sl + reg_origin

    liquid_rel_perm = BothProperties(
        with_derivatives=liquid_rel_perm_with_derivatives,
        without_derivatives=liquid_rel_perm_without_derivatives,
    )

    return [gas_rel_perm, liquid_rel_perm]
