import numpy as np
from compass_utils.units import bar, MPa
from prop_library.viscosities import liquid_diphasic_viscosities
from physics.physical_state import PhaseStateStruct
from physics.fluid_physical_properties import CompiledBothProperties


# Test the implementation directly in python (without
# loading a physics)
def test_call_viscosity():

    p = 1.0 * bar
    T = 280.0
    C = np.array([0.0, 1.0])

    # define the type of Xalpha (needs the number of components)
    phase_state_type = PhaseStateStruct(2)
    X = phase_state_type.Xalpha(p, T, C)
    dfdX = phase_state_type.empty_Xalpha()
    val_without_derivatives = liquid_diphasic_viscosities.without_derivatives(X)
    val_with_derivatives = liquid_diphasic_viscosities.with_derivatives(X, dfdX)
    assert np.allclose(val_without_derivatives, val_with_derivatives)
    print(
        "without loading a physics the viscosity is ",
        val_with_derivatives,
        " and the derivatives are ",
        dfdX,
    )
    # Create the object to use the vectorize function
    compiled_liquid_diphasic_viscosities = CompiledBothProperties(
        phase_state_type,
        liquid_diphasic_viscosities.with_derivatives,
        liquid_diphasic_viscosities.without_derivatives,
    )

    npts = 5
    Xarray = np.empty(npts, dtype=phase_state_type.Xt)
    Xarray["pressure"] = np.logspace(np.log10(bar), np.log10(50 * MPa), npts)
    Xarray["temperature"] = np.linspace(273.15, 573.15, npts)
    Xarray["molar_fractions"] = np.array(
        [np.linspace(0.0, 1.0, npts), 1 - np.linspace(0.0, 1.0, npts)]
    ).T
    print(
        "Using the vectorize property ",
        compiled_liquid_diphasic_viscosities.vect_without_derivatives(Xarray),
    )
