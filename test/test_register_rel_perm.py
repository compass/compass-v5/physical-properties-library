from physics import Diphasic
from physics.rock_physical_properties import RockProperties
from physics.set_relative_permeability import (
    set_relative_permeability_functions,
)


def test_cpp_register_rel_perm(the_physics=Diphasic()):
    def register_diphasic_rel_perm(phy_properties, rocktype_id):
        from physics.utils import load_library

        lib = load_library(None, "relative_permeabilities")
        square_rel_perm = [
            lib.gas_diphasic_rel_perm,
            lib.liquid_diphasic_rel_perm,
        ]
        set_relative_permeability_functions(
            phy_properties, rocktype_id, square_rel_perm
        )
        return phy_properties

    for rocktype_id in [0, 2, 37, 0]:
        the_rock_properties = RockProperties(the_physics)
        register_diphasic_rel_perm(the_rock_properties, rocktype_id)
